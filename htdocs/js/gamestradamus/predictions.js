define([
    "dojo/on",
    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/query",
    "atemi/io/jsonrpc",
    "bootstrap/Tooltip",
    "dojo/domReady!"
], function(on, array, event, dom, domClass, query, jsonrpc, tooltip) {

    var init = function () {
        var rpc = jsonrpc('/jsonrpc');
        var now = (new Date()).getTime();
        var timer;
        var are_predictions_updated = function() {
            rpc.request({
                method: 'predictions.last-predictions-update',
                params: []
            }, function(data) {
                var last_update = Date.parse(data);
                if (!isNaN(last_update) && last_update > now) {
                    clearInterval(timer);
                    window.location.reload();
                }
            });
        };

        // If there is a prophesy call running when the page is loaded, 
        // wait until the operation is finished and reload the page.
        var update_predictions_loader = dom.byId('update-predictions-loader');
        if (!domClass.contains(update_predictions_loader, 'hidden')) {
            timer = setInterval(are_predictions_updated, 1500);
        }

        var update_predictions = dom.byId('update-predictions');
        if (update_predictions) {
            on(update_predictions, 'click', function(evt) {
                event.stop(evt);

                domClass.add(dom.byId('last-updated-wrapper'), 'hidden');
                domClass.remove(dom.byId('update-predictions-loader'), 'hidden');

                rpc.request({
                    method: 'predictions.update-predictions',
                    params: []
                }, function(data) {
                    timer = setInterval(are_predictions_updated, 1500); 
                });
            }); 
        }

        // Show the account the last time their predictions were updated.
        var last_updated = dom.byId('last-updated');
        var date = Date.parse(last_updated.getAttribute('data-last-updated'));
        if (!isNaN(date)) {
            date = new Date(date);
            var time;
            if (date.getHours() == 0 && date.getMinutes() == 0) {
                time = '12 midnight';
            } else if (date.getHours() == 12 && date.getMinutes() == 0) {
                time = '12 noon';
            } else if (date.getHours() == 0) {
                time = '12:' + ('0' + date.getMinutes()).slice(-2) + ' am';
            } else if (date.getHours() < 12) {
                time = date.getHours() + ':' + ('0' + date.getMinutes()).slice(-2) + ' am';
            } else if (date.getHours() < 13) {
                time = date.getHours() + ':' + ('0' + date.getMinutes()).slice(-2) + ' pm';
            } else {
                time = (date.getHours() - 12) + ':' + ('0' + date.getMinutes()).slice(-2) + ' pm';
            }
            last_updated.innerHTML = 
                ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'][date.getMonth()]
                + ' ' + date.getDate() + ', ' + date.getFullYear() + ' at ' + time;
        } else {
            last_updated.innerHTML = 'never';
        }


        var value_tip_pos = "top";
        var hours_tip_pos = "top";

        // Setup closest games tooltips
        // Tooltip showing the hour breakdown.
        array.forEach(query(".left-column a"), function(element) {
            var game_1_name = element.getAttribute('data-closest-game-name-1');
            var game_1_value = element.getAttribute('data-closest-game-value-1');
            var game_2_name = element.getAttribute('data-closest-game-name-2');
            var game_2_value = element.getAttribute('data-closest-game-value-2');
            var game_need_more_data = element.getAttribute('data-need-more-data');
            var message = null;
            if (game_need_more_data == "true") {
                message = "<strong>Understanding the predictions:</strong><br> " +
                        " In order to understand this cost per hour " +
                        " you should enter the amount you paid for " +
                        " the games you own in <strong>Your Library</strong>.";
            } else if (game_1_name && game_2_name) {
                message = "Games you own with the closest cost per hour: <span><strong>" + 
                    game_1_name + "</strong> at  " + game_1_value + 
                    "</span> and <span><strong> " + game_2_name + "</strong> at " + 
                    game_2_value + "</span>";
            }
            if (!message) {
                return;
            }
            query(element).tooltip({
                animation: false,
                trigger: 'hover',
                placement: value_tip_pos,
                html: true,
                title: message
            });
        });
        array.forEach(query(".right-column a"), function(element) {
            var game_1_name = element.getAttribute('data-closest-game-name-1');
            var game_1_hours = element.getAttribute('data-closest-game-hours-1');
            var game_2_name = element.getAttribute('data-closest-game-name-2');
            var game_2_hours = element.getAttribute('data-closest-game-hours-2');
            var game_need_more_data = element.getAttribute('data-need-more-data');
            var message = null;
            if (game_need_more_data == "true") {
                message = "<strong>Understanding the predictions:</strong><br> " +
                        " You need to play more games!  There are some great games " +
                        " available on steam. Go forth and game!";
            } else if (game_1_name && game_2_name) {
                message = "Games you own with the closest play time: <span><strong>" + 
                    game_1_name + "</strong> at  " + game_1_hours + 
                    "</span> and <span><strong> " + game_2_name + "</strong> at " + 
                    game_2_hours + "</span>";
            }
            if (!message) {
                return;
            }
            query(element).tooltip({
                animation: false,
                trigger: 'hover',
                placement: hours_tip_pos,
                html: true,
                title: message
            });
        });
    };
    return init;
});
