define([
    "dojo/query",
    "dojo/_base/array",
    "atemi/util/TableSort",
    "atemi/util/format",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/NodeList-dom",
    "dojo/domReady!"
], function(query, array, TableSort, format, dom, domClass) {

    var init = function() {
        var table = dom.byId('game_list');
        var games = query('tbody tr.game', table);

        // Tabulate the values displayed in the table.
        var tabulate = function() {
            var hours_played = 0.00;
            var dollars_paid = 0.00;
            var cost_per_hour = 0.00;
            var rating = 0.00;

            var count = 0;
            var rating_count = 0;

            array.forEach(games, function(item) {
                if (domClass.contains(item, 'ignore')) {
                    return;
                }

                hours_played = hours_played + parseFloat(item.cells[2].textContent);
                dollars_paid = dollars_paid + parseFloat(item.cells[3].textContent.replace('$', ''));
                count++;

                var value = parseInt(query('div.rating', item.cells[5])[0].getAttribute('data-rating'));
                if (value > 0) {
                    rating = rating + value;
                    rating_count++;
                }
            });

            var item = query('tfoot tr', table)[0];
            item.cells[2].innerHTML = format.comma_separated(hours_played.toFixed(2));
            item.cells[3].innerHTML = format.comma_separated(dollars_paid.toFixed(2));
            if (dollars_paid > 0) {
                item.cells[4].innerHTML = (dollars_paid / hours_played).toFixed(2);
            } else {
                item.cells[4].innerHTML = '&mdash;';
            }
            if (rating_count > 0) {
                item.cells[5].innerHTML = (rating / rating_count).toFixed(1);
            } else {
                item.cells[5].innerHTML = '&mdash;';
            }

            // Update the global statistics.
            var stat;
            stat = query('div.average-cost-per-hour div.stat')[0];
            if (hours_played > 0) {
                if (Math.round((dollars_paid * 100) / hours_played) >= 100) {
                    stat.innerHTML = '<span class="unit">$</span>' + 
                        (dollars_paid / hours_played).toFixed(2);
                } else {
                    stat.innerHTML = ((dollars_paid / hours_played) * 100).toFixed(0) +
                        '<span class="unit">¢</span>';
                }
            } else {
                stat.innerHTML = '0<span class="unit">¢</span>';
            }

            stat = query('div.total-cost div.stat')[0];
            stat.innerHTML = format.comma_separated(dollars_paid.toFixed(2));

            stat = query('div.total-playtime div.stat')[0];
            stat.innerHTML = format.comma_separated(hours_played.toFixed(1));

            stat = query('div.total-count div.stat')[0];
            stat.innerHTML = format.comma_separated(count.toFixed(0));

            stat = query('div.average-cost div.stat')[0];
            stat.innerHTML = (dollars_paid / count).toFixed(2);

            stat = query('div.average-playtime div.stat')[0];
            stat.innerHTML = (hours_played / count).toFixed(1);

            if (rating_count > 0) {
                stat = query('div.average-rating div.stat')[0];
                stat.innerHTML = (rating / rating_count).toFixed(1);
            }
        }
        tabulate();

        var game_list_anchor = query("#game_list #cost-per-hour")[0];
        new TableSort(
            'game_list', [0, 0], [
                TableSort.textContent,
                TableSort.textContent,
                TableSort.parseFloat,
                function(td) {
                    return parseFloat(td.textContent.replace('$', '').replace(',', ''));
                },
                function(td) {
                    var order = domClass.contains(
                        game_list_anchor, 'sort-descending');
                    var value = parseFloat(td.textContent);
                    if (isNaN(value)) {
                        if (!order) {
                            return -Infinity;
                        } else {
                            return Infinity;
                        }
                    }
                    return value;
                },
                function(td) {
                    var rating = query('div.rating', td)[0];
                    return parseInt(rating.getAttribute('data-rating'));
                } 
            ],
            function(data) {
                // Fix the position counter for the rows.
                array.forEach(data, function(row, i) {
                    row[0].cells[0].innerHTML = (i + 1);
                });
            }
        ); 
    };
    return init;
})
