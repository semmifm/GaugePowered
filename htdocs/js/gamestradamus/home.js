define([
    "bootstrap/Tooltip",

    "dojo/_base/array",
    "dojo/_base/event",
    "dojo/dom",
    "dojo/dom-class",
    "dojo/on",
    "dojo/mouse",
    "dojo/query",

    "dojo/NodeList-dom"
], function(Tooltip, array, event, dom, domClass, on, mouse, query) {
    var init = function() {
        on(query('#dots a'), 'click', function(evt) {
            event.stop(evt);

            query('#dots a').removeClass('active');
            domClass.add(evt.currentTarget, 'active');

            query('section.feature').addClass('hidden');
            domClass.remove(
                dom.byId(evt.currentTarget.getAttribute('data-feature')),
                'hidden'
            ); 
        }); 

        var open = false;
        var container = query('body')[0];
        var privacy = query('.header .privacy a')[0];
        query(privacy).tooltip({
            container: container,
            trigger: 'manual',
            placement: 'right',
            html: true,
            title: '<p>Gauge uses the publicly available data about your Steam account from the Steam community website.' + 
                '<br><br>Signing in only allows Gauge to identify you within the Steam community.' + 
                '<br><br><a href="/faq/">Read more...</a></p>' 
        });
        on(privacy, mouse.enter, function(evt) {
            if (!open) {
                open = true;
                query(privacy).tooltip('show');
            }
        });

        on(window, 'resize', function(evt) {
            if (open) {
                open = false;
                query(privacy).tooltip('hide');
            }
        });
    };
    return init;
});
