"""Gamestradamus plugins.
"""
import cherrypy
from cherrypy.process import plugins
from jamaisvu import pglock

from plumbing.email import units
from plumbing.email.backends import stdout
from gamestradamus import cli

#-------------------------------------------------------------------------------
class DEV_SendEmailMonitor(plugins.Monitor):
    """Monitor that periodically sends out emails.

    ONLY use this in dev, it has the potential to send out multiple emails
    """

    #---------------------------------------------------------------------------
    def __init__(self, bus, frequency=30):
        plugins.Monitor.__init__(self, bus, self.run, frequency)

    #---------------------------------------------------------------------------
    def run(self):
        if not hasattr(cherrypy.Application, 'store'):
            self.bus.log(
                "No storage manager is attached to the Application."
            )
            return

        sandbox = cherrypy.Application.store.new_sandbox()
        self.bus.log("Sending emails")
        with pglock.PGAdvisoryLock(sandbox, "GAUGE-EMAIL-QUEUE"):
            emails = units.Email.email_queue(sandbox)
            stdout.process_queue(None, emails)
        self.bus.log("Done");
        sandbox.flush_all()
